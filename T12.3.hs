--Relevante Datentypen
type Day = Int
type Month = Int
type Year = Int

--Alle Monate mit 31 Tagen
list = [1,3,5,7,8,10,12]

--Datentyp Date
data Date = MkDate {day :: Day,
            month :: Month,
            year :: Year}
			deriving(Show)

--Prüft ob ein Datum valide ist
isValidDate :: Date -> Bool
isValidDate date = (isValidDay (day date)) && (isValidMonth (month date)) && (isValidYear (year date)) where
    isValidYear year = year >= 2000 && year <= 2100
    isValidDay day = day >= 1 && day <= (getDays (month date) (year date))
    isValidMonth month = month >= 1 && month <= 12
    
--Prüft ob ein Jahr ein Schaltjahr ist
isSchaltjahr :: Year -> Bool
isSchaltjahr year = year `mod` 4 == 0

--Gibt an wieviele Tage der gegebene Monat im gegebenen Jahr hat
getDays :: Month -> Year -> Int
getDays month year = if(month == 2) then if(isSchaltjahr year) then 29 else 28
                        else if(month `elem` list) then 31 else 30
                        
--Berechnet die Anzahl vergangener Tage bis zum ersten eines Monats                     
daysToMonthFrist :: Month -> Year -> Int
daysToMonthFrist month year
                    | month == 1 && year == 2000 = 0
                    | month == 1 = (getDays 12 (year - 1)) + (daysToMonthFrist 12 (year - 1))
                    | otherwise = (getDays (month - 1) year) + (daysToMonthFrist (month - 1) year)

--Berechnet den ersten Sonntag im Monat
getRentDay :: Date -> Date
getRentDay date 
                | fromIntegral (daysToMonthFrist (month date) (year date)) `mod` 7 == 1 = date
                | fromIntegral (daysToMonthFrist (month date) (year date)) `mod` 7 > 1 = (MkDate (9 - fromIntegral (daysToMonthFrist (month date) (year date)) `mod` 7) (month date) (year date))
                | fromIntegral (daysToMonthFrist (month date) (year date)) `mod` 7 < 1 = (MkDate 2 (month date) (year date))